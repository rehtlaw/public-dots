;;; config.el -*- lexical-binding: t; -*-

;; visuals
(setq doom-font (font-spec :family "Source Code Pro" :size 14)
      doom-theme 'rehtlaw-evergarden
      which-key-idle-delay 0.1)
(setq-default word-wrap t)
(global-visual-line-mode 1)
(menu-bar-mode 1)

;; misc
(setq-default neo-show-hidden-files 'nil)
(setenv "GOPATH" "/home/jonas/go")

;; Bindings
(map! [remap evil-jump-to-tag]     #'projectile-find-tag
      [remap find-tag]             #'projectile-find-tag
      [remap newline]              #'newline-and-indent

      :nmvo doom-leader-key nil
      :nmvo doom-localleader-key nil

      ;; ----------- CUSTOM BINDS -------------------------

      :desc "Toggle neotree open"            "<f8>" #'+treemacs/toggle

      (:leader
        :desc "Replace M-x"                  :nve "<SPC>" #'counsel-M-x
        :desc "Comment line(s)"              :nve "," #'comment-line
        (:desc "file" :prefix "f"
          :desc "Find file"                  :nve "f" #'counsel-find-file)
        (:desc "buffer" :prefix "b"
          :desc "List all buffers"           :nve "l" #'switch-to-buffer
          :desc "Kill Buffer list"           :nve "k" #'kill-buffer)
        :desc "open up eshell buffer"        :nve "'" #'+eshell/open-popup
        (:desc "split" :prefix "s"
          :desc "Split window horizontally"  :nve "h" #'split-window-horizontally
          :desc "Split window vertically"    :nve "v" #'split-window-vertically)
      )
      :nve "/" #'swiper
)

;; Orgmode
(setq +org-dir (expand-file-name "/home/jonas/Nextcloud/org-md/org"))

;; Webmod
(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.vue?\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.ejs?\\'" . web-mode))

(provide 'config)
;;; config.el ends here
