# rehtlaw's public dotfiles

These are all my configs that are ment for the public eye.  
I should note that a lot is stolen from [this
guy](https://github.com/addy-dclxvi), so give him the credit for things, not me
:D  
I browsed through my wallpaper collection and found this nice picture, so I decided to take some time and build a rice around it.
I used pywal and the monokai colorscheme as a base for the colors. I ended up
using [this website](https://meyerweb.com/eric/tools/color-blend/#:::hex) to blend
monokai's colors with the browntones that pywal gave me. 

## my setup

I have 2 computers that I use on a regular basis:
* a Thinkpad T430 (1600x900 screen) running mainly HerbstluftWM
* a self-built tower/cube PC (2x 1920x1080 monitors) running mainly XFCE4   
so you will see some configs that are only used or run on a specific machine (e.g. the 2 different conky and tint2 configs and 2 herbstluftwm scripts only ment for the desktop or laptop)

## programs

* __herbstluftwm__ as my tiling window manager
* __xfce4__ as my floating window manager (mainly using it because OBS and games play nicer with it than with hlwm)
* __tint2__ as my statusbar for XFCE currently
* __polybar__ as my statusbar for HLWM
* __conky__ as a general status indicator
* __dunst__ for my notifications
* __compton__ as my compositor (although I'm currently not using the config provided with this, but rather the out-of-the-box configuration)
* __termite__ as my terminal emulator (although I'm trying to switch to st... just need some time to configure it)
* __thunar__ as my file browser
* [__betterlockscreen__](https://github.com/pavanjadhaw/betterlockscreen) for my lockscreen and wallpaper
* __vim__ for basic config editing
* [__DOOM Emacs__](https://github.com/hlissner/doom-emacs) for code editing
* [__ungoogled-chromium__](https://github.com/Eloston/ungoogled-chromium) as my web browser
* either __neomutt__ + __offlineimap__ (using [Luke's setup script(s)](https://github.com/LukeSmithxyz/mutt-wizard)) or __Rainloop__ for my e-mails
* __mpd__ + __ncmpcpp__ for my music
* __mpv__ (and __VLC__ as backup) for my video player

## screenshots
these screenshots are from my herbstluftwm config and were taken on my Thinkpad T430
![clean](https://gitlab.com/rehtlaw/public-dots/raw/master/.imgs/clean.png)
__clean__
* Wallpaper by [an unknown artist (atleast I couldn't find him/her :( )](https://wall.alphacoders.com/big.php?i=904939)
* Now playing: [_fresh clean smooth fly [ x shamana ]_ by _bsd.u_](https://radiojuicy.bandcamp.com/album/late-night-bumps)

![termite](https://gitlab.com/rehtlaw/public-dots/raw/master/.imgs/termite.png)
__termite__
* I'm using the `colorbar` script from [Addy's .toys folder](https://github.com/addy-dclxvi/almighty-dotfiles/tree/master/.toys)
* Now playing: [_Myriad Colors_ by _D I V I N I T Y_](https://divinityofficialpage.bandcamp.com/album/ukiyo-reissue)

![emacs](https://gitlab.com/rehtlaw/public-dots/raw/master/.imgs/emacs.png)
[__DOOM Emacs__](https://github.com/hlissner/doom-emacs)
* This is a really nice alternative to spacemacs. Easy to set up and super fast bootup times.
* Emacs here is showing a simple python file for a discord bot of mine
* Now Playing: [_Prologue_ by _Chris Remo_](https://camposantogames.bandcamp.com/album/firewatch-original-score)

![rofi](https://gitlab.com/rehtlaw/public-dots/raw/master/.imgs/rofi.png)
__rofi__
* I took the rofi script from
  [addy](https://github.com/addy-dclxvi/almighty-dotfiles/blob/master/.utility/rofi-apps)
  and changed the theme and font to fit my rice.
* Now Playing: [_Harlem River_ by _Moderator_](https://moderator.bandcamp.com/album/the-world-within)
