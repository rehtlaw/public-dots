#!/bin/bash
Mod=Mod4

# disable capslock
setxkbmap -option ctrl:nocaps

################## Misc #######################
hc keybind $Mod-Shift-Escape spawn systemctl poweroff
hc keybind $Mod-Shift-r reload
hc keybind $Mod-Shift-q close
hc keybind $Mod-c spawn betterlockscreen --lock dimblur
hc keybind $Mod-i jumpto urgent

################# Frames ######################
# focusing clients
hc keybind $Mod-h     focus left
hc keybind $Mod-j     focus down
hc keybind $Mod-k     focus up
hc keybind $Mod-l     focus right

# moving clients
hc keybind $Mod-Shift-h     shift left
hc keybind $Mod-Shift-j     shift down
hc keybind $Mod-Shift-k     shift up
hc keybind $Mod-Shift-l     shift right

# splitting frames
# create an empty frame at the specified direction
hc keybind $Mod-u       split   bottom  0.5
hc keybind $Mod-o       split   right   0.5
# let the current frame explode into subframes
hc keybind $Mod-Control-space split explode

# resizing frames
resizestep=0.05
hc keybind $Mod-Control-h       resize left +$resizestep
hc keybind $Mod-Control-j       resize down +$resizestep
hc keybind $Mod-Control-k       resize up +$resizestep
hc keybind $Mod-Control-l       resize right +$resizestep

# layouting
hc keybind $Mod-r remove
hc keybind $Mod-s floating toggle
hc keybind $Mod-f fullscreen toggle
hc keybind $Mod-p pseudotile toggle
# The following cycles through the available layouts within a frame, but skips
# layouts, if the layout change wouldn't affect the actual window positions.
hc keybind $Mod-space                                                           \
            or , and . compare tags.focus.curframe_wcount = 2                   \
                     . cycle_layout +1 vertical horizontal max vertical grid    \
               , cycle_layout +1

## Tags
tag_names=( {1..9} )
tag_keys=( {1..9} )
hc rename default "${tag_names[0]}" || true
for i in ${!tag_names[@]} ; do
    hc add "${tag_names[$i]}"
    key="${tag_keys[$i]}"
    if ! [ -z "$key" ] ; then
        hc keybind "$Mod-$key" use_index "$i"
        hc keybind "$Mod-Shift-$key" move_index "$i"
    fi
done

# cycle through tags
hc keybind $Mod-period use_index +1 --skip-visible
hc keybind $Mod-comma  use_index -1 --skip-visible

################## Mouse ######################
hc mousebind $Mod-Button1 move
hc mousebind $Mod-Button2 resize
hc mousebind $Mod-Button3 zoom

################# PROGRAMS ####################
hc keybind $Mod-Return spawn termite
hc keybind $Mod-n spawn termite -e ncmpcpp
hc keybind $Mod-m spawn termite -e neomutt

hc keybind $Mod-t spawn thunar

hc keybind $Mod-d spawn ~/.bin/rofi-apps

hc keybind $Mod-Down spawn mpc volume -5
hc keybind $Mod-Up spawn mpc volume +5

############### FUNCTION KEYS ################
# Pulse Audio controls
hc keybind XF86AudioRaiseVolume spawn pactl set-sink-volume 0 +5% #increase sound volume
hc keybind XF86AudioLowerVolume spawn pactl set-sink-volume 0 -5% #decrease sound volume
hc keybind XF86AudioMute spawn pactl set-sink-mute 0 toggle # mute sound
hc keybind XF86AudioMicMute spawn pactl set-source-mute alas_input.pci-0000_00_1b.0.analog-stereo toggle

# Sreen brightness controls
hc keybind XF86MonBrightnessUp spawn xbacklight -inc 10 # increase screen brightness
hc keybind XF86MonBrightnessDown spawn xbacklight -dec 10 # decrease screen brightness

# Media player controls
hc keybind XF86AudioPlay spawn playerctl play
hc keybind XF86AudioPause spawn playerctl pause
hc keybind XF86AudioNext spawn playerctl next
hc keybind XF86AudioPrev spawn playerctl previous
