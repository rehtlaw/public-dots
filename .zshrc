# init zplug
source ~/.zplug/init.zsh

# set GOPATH
export GOPATH=$HOME/go
# add ~/.bin and $GOPATH/bin to PATH
export PATH=$PATH:$HOME/.bin:$HOME/.local/bin:$GOPATH/bin
# default editor for terminal stuff
export VISUAL="vim"
export EDITOR="vim"

# exporting my default browser so it can be used by scripts
export BROWSER="chromium"

# making zsh case_insensitive
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'

# pyvirtualenv stuff
export WORKON_HOME=~/.envs
source =virtualenvwrapper.sh

# Let zplug manage itself
zplug 'zplug/zplug', hook-build:'zplug --self-manage'
# zplug plugins
zplug "plugins/git", from:oh-my-zsh
zplug "plugins/npm", from:oh-my-zsh
zplug "plugins/django", from:oh-my-zsh
zplug "plugins/docker", from:oh-my-zsh
zplug "plugins/pip", from:oh-my-zsh
zplug "plugins/chucknorris", from:oh-my-zsh
zplug "zsh-users/zsh-completions", defer:1
zplug "zsh-users/zsh-autosuggestions", defer:1
zplug "zdharma/fast-syntax-highlighting"
zplug "MichaelAquilina/zsh-autoswitch-virtualenv"
# Load spaceship theme
zplug "denysdovhan/spaceship-prompt", use:spaceship.zsh, from:github, as:theme
# Install plugins if there are plugins that have not been installed
if ! zplug check --verbose; then
    printf "Install? [y/N]: "
    if read -q; then
        echo; zplug install
    fi
fi

# pyvirtualenv stuff (this has to be here for the virtualenv plugin to work)
export WORKON_HOME=~/.envs
source =virtualenvwrapper.sh

# Then, source plugins and add commands to $PATH
zplug load

# Spaceship stuff
# ORDER
SPACESHIP_PROMPT_ORDER=(
  host
  dir
  git
  # package
  time
  node
  golang
  docker
  pyenv
  exec_time
  line_sep
  jobs
  exit_code
  char
)
# CONFIG
SPACESHIP_TIME_SHOW=true
SPACESHIP_TIME_PREFIX=" "
SPACESHIP_HOST_PREFIX="@"
SPACESHIP_NODE_SUFFIX=" "
SPACESHIP_BATTERY_SHOW='always'

## package manager aliases
# alias xbps-src-help="echo ./xbps-src pkg package  sudo xbps-install -u --repository=hostdir/binpkgs/nonfree package"

### ARCH PACKAGE MANAGEMENT (with yay)
alias yay="yay --sudoloop --afterclean "
alias update="yay -Syu"
alias yays="yay -Ss"
alias remove="yay -R"

# Hugo aliases
hugo-push () {
  cd $HOME/.repos/website/
  git rm -rf .
  git clean -fxd
  cd $HOME/programming/blog-src/
  hugo -d $HOME/.repos/website/
  cd $HOME/.repos/website/
  git add .
  git commit -m "`date`"
  git push
}

# general aliases
alias ps="ps aux"
alias df="df -h"
alias ll='ls --color=auto -lah'
alias clone="git clone"
alias zshrc="vim ~/.zshrc"
alias pipes="pipes.sh -R -r 5000"
alias mp3dl='youtube-dl -x --audio-format mp3 --audio-quality 0 -o "%(title)s.%(ext)s"'
alias scdl='youtube-dl --audio-quality 0 -o "%(uploader)s - %(title)s.%(ext)s"'
alias anaconda_load='source /opt/anaconda/bin/activate root'

#motd
chuck_cow
